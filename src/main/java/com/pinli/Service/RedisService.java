package com.pinli.Service;

import java.util.List;
import java.util.Set;

public interface RedisService {


    /**
     * 写入缓存到redis
     *
     * @param key   键
     * @param value 值
     * @return boolean OK:写入成功
     */
    boolean set(String key, Object value);


    /**
     * 读取缓存
     *
     * @param key    键
     * @param tClass 要获取的数据类型的Class
     * @param <T>    要获取的数据类型
     * @return object对象 出现异常或key为null以及 值不存在 返回null
     */
    public <T> T getObject(String key, Class<T> tClass);

    /**
     * 写入缓存并设置有效时间
     *
     * @param key        主键
     * @param value      值
     * @param expireTime 有效时间单位 秒
     * @return boolean true:写入成功   false:写入失败
     */
    boolean setEx(final String key, Object value, int expireTime);

    /**
     * 验证key
     *
     * @param key
     * @return
     */
    boolean hasKey(String key);



    /**
     * 设置多少秒后过期
     *
     * @param key        主键
     * @param expireTime 过期时间 秒
     * @return
     */
    public boolean expire(String key, int expireTime);

    /**
     * 删除缓存对应的value
     *
     * @param key 主键
     * @return boolean true删除成功 false 不存在对应的key 或删除失败
     */
    boolean remove(String key);


    /**
     * 写入缓存的redis 的set数据中
     *
     * @param key   set集合的键
     * @param value 保存到set的值
     * @return 1添加成功 和 0添加失败重复添加无效
     */
    boolean addRedisSet(String key, String value);


    /**
     * 获取缓存 redis 的set集合
     *
     * @param key set的键
     * @return set集合
     */
    Set<String> getRedisSet(final String key);

    /**
     * @param key
     * @return
     */
    Long getRedisSetSize(final String key);


    /**
     * 添加Object对象 到 redis 为hash类型数据 并排除 serialVersionUID 属性
     * obj 中的属性只能为基础数据类型 或String
     *
     * @param key 主键
     * @param obj 对象
     * @throws Exception
     */
    void addHash(String key, Object obj) throws Exception;

    /**
     * 获取添加到 redis的hash类型 object对象
     *
     * @param key    键
     * @param tClass 要获取的obj对象
     * @return 返回object
     * @throws Exception
     */
    public <T> T getObjectByRedis(String key, Class<T> tClass) throws Exception;


    void hset(String key, String field, String value);

    String hget(String key, String field);

    /**
     * 前缀匹配keys
     *
     * @param key
     * @return
     */
    String[] keysPre(String key);


    public boolean addList(final String key, List<String> value, Long expireTime);

    List<String> getList(String key);
    boolean setBytes(String key, byte[] bytes);

    byte[] getBytes(String key);
}
