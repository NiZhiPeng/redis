package com.pinli.Service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.pinli.util.JSONUtils;
import com.pinli.util.RedisUtil;
import com.pinli.util.StringMethodUtil;
import com.pinli.util.StringUtils;

@Service
public class RedisServiceImpl implements RedisService {

    /**
     * 根据key值获取数据库编号
     *
     * @param key 键
     * @return
     */
    private String getDb(String key) {



      /*  int db = redisConfigMulti.getDb().get(0);
        if (key.contains(EnumRedisPrefix.USER.getProperty())) {
            db = redisConfigMulti.getDb().get(1);
        } else if (key.contains(EnumRedisPrefix.LOG.getProperty())) {
            db = redisConfigMulti.getDb().get(2);
        } else if (key.contains(EnumRedisPrefix.CITY.getProperty())) {
            db = redisConfigMulti.getDb().get(3);
        } else if (key.contains(EnumRedisPrefix.JOB.getProperty())) {
            db = redisConfigMulti.getDb().get(2);
        } else if (key.contains(EnumRedisPrefix.VITAE.getProperty())) {
            db = redisConfigMulti.getDb().get(2);
        }*/

        return null;
    }

    // redis 基础操作=========================================================================

    /**
     * 检查key是否存在
     *
     * @param key 主键
     * @return 有 true 没有 false
     */
    @Override
    public boolean hasKey(String key) {
        if (StringUtils.isNullOrEmpty(key)) return false;
        return RedisUtil.exists(key);
    }

    /**
     * 设置多少秒后过期
     *
     * @param key        主键
     * @param expireTime 过期时间 秒
     * @return false或 true
     */
    @Override
    public boolean expire(String key, int expireTime) {
        if (StringUtils.isNullOrEmpty(key)) return false;
        //返回1 说明设置成功
        return contrastResult(RedisUtil.expire(key, expireTime));
    }


    /**
     * 比较结果 对比redis操作中 结果为1 就是操作成功
     *
     * @param result Long redis操作返回的数字结果
     * @return
     */
    private static boolean contrastResult(Long result) {
        if (result != null && result == 1L) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 删除 一个key 值
     *
     * @param key 主键
     * @return 删除成功返回 1 key不存在 返回0
     */
    @Override
    public boolean remove(String key) {
        if (StringUtils.isNullOrEmpty(key)) return false;
        return contrastResult(RedisUtil.del(key));
    }


    /**
     * 前缀匹配keys
     *
     * @param key
     * @return
     */
    @Override
    public String[] keysPre(String key) {
        Set<String> keys = RedisUtil.keys(key);
        return keys.stream().toArray(String[]::new);
    }
    /*====================== redis 基础操作 end =============================*/


    /*=========== redis String数据操作方法 begin=================*/

    /**
     * 写入缓存到redis
     *
     * @param key   键
     * @param value 值
     * @return boolean true:写入成功   false:写入失败
     */
    @Override
    public boolean set(String key, Object value) {
        String val = JSONUtils.objectToJson(value);
        if (StringUtils.isNullOrEmpty(key) || StringUtils.isNullOrEmpty(val)) {
            return false;
        }

        if (StringUtils.isNullOrEmpty(getDb(key))) {
            return contrastResultString(RedisUtil.set(key, val));
        } else {
            return contrastResultString(RedisUtil.set(getDb(key), key, val));
        }

    }

    /**
     * 读取缓存
     *
     * @param key    键
     * @param tClass 要获取的数据类型的Class
     * @param <T>    要获取的数据类型
     * @return object对象 出现异常或key为null以及 值不存在 返回null
     */
    public <T> T getObject(String key, Class<T> tClass) {
        if (hasKey(key)) {
            return JSONUtils.jsonToObject(RedisUtil.get(key), tClass);
        } else {
            return null;
        }
    }

    /**
     * 处理redis 操作返回的String类型结果
     *
     * @param
     * @return
     */
    private static boolean contrastResultString(String result) {
        if (!StringUtils.isNullOrEmpty(result) && result.equals("OK")) {
            return true;
        } else {
            return false;
        }
    }



    @Override
    public boolean setEx(String key, Object value, int expireTime) {
        boolean result = false;
        String val = JSONUtils.objectToJson(value);
        if (StringUtils.isNullOrEmpty(key) || StringUtils.isNullOrEmpty(val)) {
            return result;
        }

        if (StringUtils.isNullOrEmpty(getDb(key))) {
            result = contrastResultString(RedisUtil.set(key, val));
        } else {
            result = contrastResultString(RedisUtil.set(getDb(key), key, val));
        }
        if (result) {
            result = contrastResult(RedisUtil.expire(key, expireTime));
        }
        return result;
    }



    /*=========== redis String数据操作方法 end  =================*/


    /**
     * 写入缓存的redis 的set数据中
     *
     * @param key   set集合的键
     * @param value 保存到set的值
     * @return true添加成功 和 false添加失败重复添加无效
     */
    @Override
    public boolean addRedisSet(String key, String value) {
        if (StringUtils.isNullOrEmpty(key) || StringUtils.isNullOrEmpty(value)) {
            return false;
        }

        return contrastResult(RedisUtil.sadd(key, value));
    }

    /**
     * 获取缓存 redis 的set集合
     *
     * @param key set的键
     * @return set集合
     */
    @Override
    public Set<String> getRedisSet(String key) {
        if (hasKey(key)) {
            return RedisUtil.smembers(key);
        } else {
            return null;
        }
    }

    /**
     * 获取redis set集合的数量
     *
     * @param key 键
     * @return 数量
     */
    @Override
    public Long getRedisSetSize(String key) {
        if (hasKey(key)) {
            return RedisUtil.scard(key);
        } else {
            return null;
        }
    }


    @Override
    public void addHash(String key, Object obj) throws Exception {
        if (StringUtils.isNullOrEmpty(key) || obj == null) {
            return;
        }
        Class zClass = obj.getClass();
        Field[] fields = zClass.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (!fieldName.equals("serialVersionUID")) {
                field.setAccessible(true);
                PropertyDescriptor pd = new PropertyDescriptor(fieldName, zClass);
                //通过方法名获取get方法
                Method method = pd.getReadMethod();
                this.hset(key, fieldName, method.invoke(obj).toString());
            }
        }
    }

    /**
     * 获取添加到 redis的hash类型 object对象
     *
     * @param key    键
     * @param tClass 要获取的obj对象的class 对象
     * @return 返回object
     * @throws Exception
     */
    @Override
    public <T> T getObjectByRedis(String key, Class<T> tClass) throws Exception {
        T obj = tClass.newInstance();
        Field[] fields = tClass.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (!fieldName.equals("serialVersionUID")) {
                Object value = this.hget(key, fieldName);
                field.setAccessible(true);
                if (obj != null && value != null) {
                    //通过方法名获取set方法
                    Method method = tClass.getDeclaredMethod(StringMethodUtil.setMethodName(fieldName), String.class);
                    method.invoke(obj, value); //设置属性值
                }
            }
        }
        return obj;
    }


    /**
     * 向 redis hash 类型数据中 保存一个数据
     *
     * @param key   键
     * @param field 属性
     * @param value 值
     */
    @Override
    public void hset(String key, String field, String value) {
        if (StringUtils.isNullOrEmpty(key) || StringUtils.isNullOrEmpty(field) || StringUtils.isNullOrEmpty(value)) {
            return;
        }
        RedisUtil.hset(key, field, value);
    }

    /**
     * 从 redis hash类型数据中 取出一个数据
     *
     * @param key   键
     * @param field 属性
     * @return
     */
    @Override
    public String hget(String key, String field) {
        if (StringUtils.isNullOrEmpty(key) || StringUtils.isNullOrEmpty(field)) {
            return null;
        }
        return RedisUtil.hget(key, field);
    }






    /**
     * 添加 多个 string 类型数据
     *
     * @param key        键
     * @param value      值
     * @param expireTime 过时时间
     * @return
     */
    public boolean addList(final String key, List<String> value, Long expireTime) {
        if (StringUtils.isNullOrEmpty(key)) {
            return false;
        }
        if (hasKey(key)) {
            remove(key);
        }
        String[] str = new String[value.size()];
        value.toArray(str);
        RedisUtil.lpush(key, str);
        //如果有过时时间 ,设置过时时间
        if (expireTime > 0) {
            expire(key, expireTime.intValue());
        }
        return true;
    }


    public List<String> getList(String key) {
        if (StringUtils.isNullOrEmpty(key)) {
            return null;
        }
        return RedisUtil.lrange(key, 0, RedisUtil.llen(key));
    }

    //=======================


    @Override
    public boolean setBytes(String key, byte[] bytes) {
        if (StringUtils.isNullOrEmpty(key)) {
            return false;
        }
        if (this.hasKey(key)) {
            this.remove(key);
        }
        RedisUtil.lpush(key.getBytes(), bytes);
        return true;
    }

    @Override
    public byte[] getBytes(String key) {
        byte[] bytes = new byte[0];
        if (hasKey(key)) {
            List<byte[]> list = RedisUtil.lrange(key.getBytes(), 0, RedisUtil.llen(key));
            bytes = list.get(0);
        }
        return bytes;
    }

}
