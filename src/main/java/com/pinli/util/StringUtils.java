package com.pinli.util;

/**
 * 自定义的字符串操作类
 */
public class StringUtils {


    /**
     * 检查字符串是否为空格, 空("")获取null
     * <pre>
     * StringUtils.isNullOrEmpty(null)      = true
     * StringUtils.isNullOrEmpty("")        = true
     * StringUtils.isNullOrEmpty(" ")       = true
     * StringUtils.isNullOrEmpty("bob")     = false
     * StringUtils.isNullOrEmpty("  bob  ") = false
     * </pre>
     * @param cs 要检查的字符串,可以为null
     * @return {null,"","  "} 返回 true 否则为 false
     */
    public static boolean isNullOrEmpty(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            /* cs.charAt(i) 返回索引处的值
             * Character.isWhitespace(int codePoint) 根据Java确定指定的字符是否为空格。
             */
            if ((Character.isWhitespace(cs.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }


}
