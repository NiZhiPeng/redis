package com.pinli.util;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redis 连接池
 * 通过读取配置初始化Redis连接池
 * 通过参数生成Redis连接池
 */
public class RedisPool {

    /**
     * Jedis 连接池
     */
    private static JedisPool pool;
    /**
     * redis 端口号
     */
    private static int port = 6379;

    /**
     * 最大连接数
     */
    private static int maxTotal = 500;

    /**
     * 最大空闲连接数
     */
    private static int maxIdle = 10;

    /**
     * 最小空闲连接数
     */
    private static int minIdle = 5;

    /**
     * 连接超时时间
     */
    private static int timeout = 5000;

    /**
     * 在取连接时测试连接的可用性
     */
    private static boolean testOnBorrow = true;

    /**
     * 在还连接时不测试连接的可用性
     */
    private static boolean testOnReturn = false;

    /**
     * redis数据库的ID实例 默认为0
     */
    public static int redisInstanceIndex = 0;


    /* ----------------------配置文件config.properties 的配置信息------------------------  */
    /* redis 配置文件的ip */
    private static String host = YWPTUtil.getYwptConfig("redisServerHost");
    private static String portStr = YWPTUtil.getYwptConfig("redisServerPort");
    //密码
    private static String auth = YWPTUtil.getYwptConfig("redisServerAuth");
    //超时时间
    private static String redisTimeout = YWPTUtil.getYwptConfig("redisTimeout");
    //配置文件的
    private static String redisMaxTotal = YWPTUtil.getYwptConfig("redisMaxTotal");
    private static String redisMaxIdle = YWPTUtil.getYwptConfig("redisMaxIdle");
    private static String redisMinIdle = YWPTUtil.getYwptConfig("redisMinIdle");
    private static Boolean redisServer = Boolean.parseBoolean(YWPTUtil.getYwptConfig("redisServer"));
    private static String redisInstanceIndexStr = YWPTUtil.getYwptConfig("redisInstanceIndex");

    static {
        //如果配置文件的最大连接数不为空或null
        if (!StringUtils.isNullOrEmpty(redisMaxTotal)) {
            try {
                maxTotal = Integer.valueOf(redisMaxTotal);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        //如果配置文件的最大空闲不为空或null
        if (!StringUtils.isNullOrEmpty(redisMaxIdle)) {
            try {
                maxIdle = Integer.valueOf(redisMaxIdle);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        //如果配置文件的最小空闲不为空或null
        if (!StringUtils.isNullOrEmpty(redisMinIdle)) {
            try {
                minIdle = Integer.valueOf(redisMinIdle);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        initPool();  //初始化连接池
    }


    /**
     * 初始化redis连接池
     */
    private static void initPool() {
        //Jedis 连接池配置
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);           //最大连接数
        config.setMaxIdle(maxIdle);             //最大空闲数
        config.setMinIdle(minIdle);             //最小空闲数
        config.setMaxWaitMillis(1000);         //

        config.setTestOnBorrow(testOnBorrow);   //在取连接时测试连接的可用性
        config.setTestOnReturn(testOnReturn);   //在还连接时不测试连接的可用性
        config.setBlockWhenExhausted(true);
        if (!StringUtils.isNullOrEmpty(host) && redisServer) {
            if (!StringUtils.isNullOrEmpty(portStr)) {
                port = Integer.parseInt(portStr);
            }
            if (!StringUtils.isNullOrEmpty(redisTimeout)) {
                timeout = Integer.parseInt(redisTimeout);
            }
            if (!StringUtils.isNullOrEmpty(auth)) {
                //创建JedisPool 实例对象  有密码添加密码
                pool = new JedisPool(config, host, port, timeout, auth);
            } else {
                //redis服务器 没有密码则不需要添加密码
                pool = new JedisPool(config, host, port, timeout);
            }
            //设置token存活时间为60秒
            System.out.println("Redis初始化完成" + maxTotal);
        } else {
            System.out.println("Redis未初始化");
        }
    }


    /**
     * 获取Jedis 实例对象
     *
     * @return Jedis对象
     */
    public static Jedis getJedis() {
        if (redisServer) {
            Jedis jedis = pool.getResource();
            System.out.println("获取jedis   " + jedis);
            jedis.setDataSource(pool);
            if (!StringUtils.isNullOrEmpty(redisInstanceIndexStr)) {
                redisInstanceIndex = Integer.parseInt(redisInstanceIndexStr);
            }
            jedis.select(redisInstanceIndex);
            return jedis;
        } else {
            return null;
        }
    }


    /**
     * 数据库编号 0-15
     *
     * @param db
     * @return
     */
    public static Jedis getJedis(String db) {
        if (redisServer) {
            Jedis jedis = pool.getResource();
            System.out.println("获取jedis   " + jedis);
            jedis.setDataSource(pool);
            if (db != null) {
                redisInstanceIndex = Integer.parseInt(db);
            }
            //切换数据库
            jedis.select(redisInstanceIndex);
            return jedis;
        } else {
            return null;
        }
    }


    /**
     * 关闭 释放 Jedis
     *
     * @param jedis
     */
    public static void close(Jedis jedis) {
        jedis.close();
        System.out.println("释放jedis   " + jedis);
    }


    /**
     * 生成 JedisPool  连接池
     *
     * @param host    redis 服务器 ip地址
     * @param portStr redis端口
     * @param auth    redis密码
     * @return JedisPool实例
     */
    public static JedisPool generateJedisPool(String host, String portStr, String auth) {
        JedisPool pool = null;
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);           //最大连接数
        config.setMaxIdle(maxIdle);             //最大空闲数
        config.setMinIdle(minIdle);             //最小空闲数
        config.setTestOnBorrow(testOnBorrow);   //在取连接时测试连接的可用性
        config.setTestOnReturn(testOnReturn);   //在还连接时不测试连接的可用性
        config.setBlockWhenExhausted(true);
        if (!StringUtils.isNullOrEmpty(host)) {
            if (!StringUtils.isNullOrEmpty(portStr)) {
                port = Integer.parseInt(portStr);
            }
            if (!StringUtils.isNullOrEmpty(redisTimeout)) {
                timeout = Integer.parseInt(redisTimeout);
            }
            //创建JedisPool 实例对象
            pool = new JedisPool(config, host, port, timeout, auth);
            //设置token存活时间为60秒
            System.out.println("Redis初始化完成");
        } else {
            System.out.println("Redis未初始化");
        }
        return pool;
    }

}