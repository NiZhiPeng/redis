package com.pinli.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * redis配置文件 config.properties 读取工具类
 */
public class RedisConfigUtil {

    private volatile static PropertiesConfiguration configuration = null;

    public RedisConfigUtil() {
    }

    /**
     * 通过 key 获取value
     *
     * @param key 主键
     * @return 主键对应的value值
     */
    public static String getConfig(String key) {
        return getInstance().getString(key);
    }

    /**
     * 获取实例
     *
     * @return 返回 PropertiesConfiguration 实例对象 configuration
     */
    private static PropertiesConfiguration getInstance() {
        if (configuration == null) {
            synchronized (RedisConfigUtil.class) {
                if (configuration == null) {
                    try {
                        configuration = new PropertiesConfiguration("redis-config.properties");
                    } catch (ConfigurationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return configuration;
    }
}
