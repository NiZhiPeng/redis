package com.pinli.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式相关类
 */
public class RegexpUtil {


    public static boolean st(String str) {
        String regexp = "^[a-z]{2,}$";

        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        System.out.println(matcher.matches());
        return matcher.matches();
    }


    /**
     *
     *
     * @param chinese
     */

    /**
     * 字符串全部匹配中文字符
     * 使用Unicode编码范围来判断汉字；这个方法不准确,因为还有很多汉字不在这个范围之内
     *
     * @param chinese 中文字符
     * @return boolean
     */
    public static boolean isChineseByRange(String chinese) {
        if (chinese == null) {
            return false;
        }
        //创建一个模式
        Pattern pattern = Pattern.compile("^[\u4e00-\u9fa5]*$");
        //pattern.matcher() 创建一个匹配器，匹配给定的 输入内容 与 此模式。
        Matcher matcher = pattern.matcher(chinese.trim());
        return matcher.find(0);

    }

    public static void main(String[] args) {
        String str1 = "a";
        String str2 = "中午ss ";
//        RegexpUtil.st(str1);
        System.out.println(RegexpUtil.isChineseByRange(str2));

    }
}
