package com.pinli.util;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.GeoRadiusParam;

/**
 * redis 控制类 单节点
 */
public class RedisUtil {

    /**
     * 判端是否连接redis
     *
     * @return boolean
     */
    public static boolean getJedisBoolean() {
        Jedis jedis = null;
        boolean result = false;
        try {
            jedis = RedisPool.getJedis();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 判断键key 是否存在
     *
     * @param key 键
     * @return boolean true存在 false不存在
     */
    public static boolean exists(String key) {
        Jedis jedis = null;
        boolean result = false;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.exists(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 删除 一个key 值
     *
     * @param key 主键
     * @return 删除成功返回 1 key不存在 返回0
     */
    public static Long del(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 设置key 的有效时间
     *
     * @param key     键
     * @param seconds 时间秒
     * @return 设置成功 返回1 返回0说明key不存在
     */
    public static Long expire(String key, int seconds) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.expire(key, seconds);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 设置key 失效时间
     *
     * @param key                   键
     * @param millisecondsTimestamp 时间 毫秒时间戳 过期时间的毫秒值
     * @return 设置成功 返回1  返回0说明key不存在
     */
    public static Long pexpireAt(String key, long millisecondsTimestamp) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.pexpireAt(key, millisecondsTimestamp);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 查询可的剩余有效时间,单位是秒
     *
     * @param key 键
     * @return 时间.当返回-2时，说明key已经不存在 <br>
     * 当返回-1时，说明key没有设置有效时间。
     */
    public static Long ttl(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.ttl(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 根据key表达式查询 所有相同的 key
     *
     * @param key key表达式 例如 * 获取全部的key
     * @return Set集合 全部的key
     */
    public static Set<String> keys(String key) {
        Jedis jedis = null;
        Set<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.keys(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 查询redis 中 key的数据类型
     *
     * @param key String
     * @return String 数据类型
     */
    public static String type(String key) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.type(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }




    /*------------- String类型 操作begin  ----------------*/

    /**
     * 向redis写入String类型的值
     *
     * @param key   键
     * @param value 值
     * @return 写入成功 返回 OK
     */
    public static String set(String key, String value) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.set(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * @param db    redis 数据库编号
     * @param key   String
     * @param value 值
     * @return
     */
    public static String set(String db, String key, String value) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis(db);
            result = jedis.set(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    public static byte[] setByte(String db, String key, String value) {
        Jedis jedis = null;
        byte[] result = null;
        try {
            jedis = RedisPool.getJedis(db);
            jedis.set(key.getBytes(), value.getBytes());
            result = jedis.get(key.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 替换String 字符串的一部分
     *
     * @param key   String 键
     * @param start int 要替换的开始位置
     * @param value String 要替换的值
     * @return Long 返回替换后字符串的长度
     */
    public static Long setrange(String key, int start, String value) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.setrange(key, start, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 通过key 获取String类型的value 值
     *
     * @param key 键
     * @return key对应的value值
     */
    public static String get(String key) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * @param db
     * @param key
     * @return
     */
    public static String get(String db, String key) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis(db);
            result = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 获取String 范围内的值 第一位与Java 的String一样是0
     *
     * @param key   String 键
     * @param start int 开始位置
     * @param end   int 结束位置
     * @return String 开始到结束位置的值
     */
    public static String getrange(String key, int start, int end) {
        Jedis jedis = null;
        String result = "";
        try {
            jedis = RedisPool.getJedis();
            result = jedis.getrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 获取String类型数据 的长度
     *
     * @param key String 键
     * @return Long 长度 0说明值 不存在
     */
    public static Long strlen(String key) {
        Jedis jedis = null;
        Long result = 0L;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.strlen(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 在String 类型尾部追加value 返回追加后的长度
     * key不存在会添加一个key
     *
     * @param key   String 键
     * @param value String 值
     * @return Long 追加后的value值的 长度
     */
    public static Long append(String key, String value) {
        Jedis jedis = null;
        Long result = 0L;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.append(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /*------------- String类型 操作end ----------------*/


    /*------------- hash类型操作begin -----------------*/

    /**
     * 向 redis 写入hash类型的数据
     * <pre>
     *   如果 key 不存在，一个新的哈希表被创建并进行 HSET 操作。
     *   如果域 field 已经存在于哈希表中，旧值将被覆盖。
     * </pre>
     *
     * @param key   键
     * @param field 属性
     * @param value 值
     * @return 如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1。<br>
     * 如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0 。<br>
     * 保存失败返回null
     */
    public static Long hset(String key, String field, String value) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hset(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 向 redis 写入多个 hash类型的数据
     *
     * @param key
     * @param value
     * @return
     */
    public static Long hset(String key, Map<String, String> value) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hset(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 从redis中获取 hash类型中的单个值
     *
     * @param key   redis 的键
     * @param field 一个hash数据的属性
     * @return 返回 filed对应的value值。 key或filed 不存在 则返回null
     */
    public static String hget(String key, String field) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hget(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 从redis中获取 hash类型中的多个值
     *
     * @param key   键
     * @param field 属性数组
     * @return field对应的value 集合 顺序与field顺序对应
     */
    public static List<String> hmget(String key, String[] field) {
        Jedis jedis = null;
        List<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hmget(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 从redis中 获取key 对应的hash类型中的 所有的数据
     *
     * @param key 键
     * @return Map<String, String> key保存的hash类型所有值
     */
    public static Map<String, String> hgetAll(String key) {
        Jedis jedis = null;
        Map<String, String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hgetAll(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 删除hash类型数据中的属性值
     *
     * @param key    键
     * @param fields 属性值
     * @return 删除的属性数量
     */
    public static Long hdel(String key, String... fields) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hdel(key, fields);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 查询出hash 的所有属性名称
     *
     * @param key 键
     * @return 返回属性值的set集合
     */
    public static Set<String> hkeys(String key) {
        Jedis jedis = null;
        Set<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.hkeys(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /*------------- hash类型操作end -----------------*/


    /*------------- set类型操作 begin ----------------- */

    /**
     * 向redis添加set集合 类型的数据 set不可重复
     *
     * @param key     键
     * @param members 值 字符串
     * @return 没有重复的新加返回 1 有重复返回0
     */
    public static Long sadd(String key, String... members) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.sadd(key, members);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 查看set的大小 set集合的值数量
     *
     * @param key 键
     * @return set的大小值
     */
    public static long scard(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.scard(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 查询set集合中的所有值
     *
     * @param key 键
     * @return
     */
    public static Set<String> smembers(String key) {
        Jedis jedis = null;
        Set<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.smembers(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;

    }
    /*------------- set类型操作 end ----------------- */

    /*------------- list数据操作begin ---------------*/

    /**
     * 在list 头部(左边)添加数据
     *
     * @param key     键
     * @param strings 值 可以多个
     * @return 添加成功返回 添加成功的数量
     */
    public static Long lpush(String key, String... strings) {
        Jedis jedis = null;
        Long reslt = null;
        try {
            jedis = RedisPool.getJedis();
            reslt = jedis.lpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return reslt;
    }


    /**
     * list 尾部追加数据
     *
     * @param key     键
     * @param strings 值 可以多个
     * @return
     */
    public static Long rpush(String key, String... strings) {
        Jedis jedis = null;
        Long reslt = null;
        try {
            jedis = RedisPool.getJedis();
            reslt = jedis.rpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return reslt;
    }


    /**
     * 在list 批量添加
     *
     * @param key   键
     * @param value 值 可以多个
     * @return 添加成功返回 添加成功的数量
     */
    public static Long lpush(byte[] key, byte[]... value) {
        Jedis jedis = null;
        Long reslt = null;
        try {
            jedis = RedisPool.getJedis();
            reslt = jedis.lpush(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return reslt;
    }


    /**
     * 从左边开始 获取list集合某个范围内的数据
     *
     * @param key   键
     * @param start index 开始位置例如  0
     * @param end   index 结束位置
     * @return 返回
     */
    public static List<String> lrange(String key, long start, long end) {
        Jedis jedis = null;
        List<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.lrange(key, start, end);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    public static List<byte[]> lrange(byte[] key, long start, long end) {
        Jedis jedis = null;
        List<byte[]> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.lrange(key, start, end);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 查询 list集合中的值 数量
     *
     * @param key 键
     * @return list 的数量
     */
    public static Long llen(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.llen(key);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }
    /*------------- list数据操作end  ---------------*/


    /*---------------- zset 类型操作 bigin -------------------------*/

    /**
     * 向 zset 有效集合中增加一条记录,如果这个值已存在，这个值对应的权重将被置为新的权重
     *
     * @param key    String
     * @param score  double 权重 排序的值
     * @param member String 要加入的值，
     * @return 状态码 1成功，0已存在member的值
     */
    public static long zadd(String key, double score, String member) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zadd(key, score, member);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }

    /**
     * 向zset集合批量添加记录
     *
     * @param key          键
     * @param scoreMembers 数据
     * @return
     */

    public static Long zadd(String key, Map<String, Double> scoreMembers) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zadd(key, scoreMembers);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1L;
    }

    /**
     * 获取 zset 集合中元素的数量
     *
     * @param key String
     * @return 如果返回0则集合不存在
     */
    public static long zcard(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zcard(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }

    /**
     * 获取zset中 指定权重区间内集合的数量
     *
     * @param key String
     * @param min double 最小排序位置
     * @param max double 最大排序位置
     */
    public static long zcount(String key, double min, double max) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zcount(key, min, max);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }


    /**
     * 从小到大 返回指定位置的zset集合元素,0为第一个元素，-1为最后一个元素
     *
     * @param key   String redis键值
     * @param start 开始位置(包含)
     * @param end   结束位置(包含)
     * @return Set<String>
     */
    public static Set<String> zrange(String key, int start, int end) {
        Jedis jedis = null;
        Set<String> set = new HashSet<String>();
        try {
            jedis = RedisPool.getJedis();
            set = jedis.zrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return set;
    }


    /**
     * 从大到小 获取给定区间的元素，原始按照权重由高到低排序
     *
     * @param key   String
     * @param start int
     * @param end   int
     * @return Set<String>
     */
    public static Set<String> zrevrange(String key, int start, int end) {
        Jedis jedis = null;
        Set<String> set = new HashSet<String>();
        try {
            jedis = RedisPool.getJedis();
            set = jedis.zrevrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return set;
    }


    /**
     * 返回指定权重区间的元素集合
     *
     * @param key String
     * @param min Double 上限权重
     * @param max Double 下限权重
     * @return
     */
    public static Set<String> zrangeByScore(String key, double min, double max) {
        Jedis jedis = null;
        Set<String> set = new HashSet<>();
        try {
            jedis = RedisPool.getJedis();
            set = jedis.zrangeByScore(key, min, max);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return set;
    }

    /**
     * 获取指定值在 zset集合 中的位置，集合排序从低到高
     *
     * @param key    String
     * @param member String
     * @return long 位置
     */
    public static long zrank(String key, String member) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zrank(key, member);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }

    /**
     * 逆序 获取指定值在  zset集合 中的位置，集合排序从高到低
     *
     * @param key    String 键
     * @param member String 值
     * @return long 位置
     */
    public static long zrevrank(String key, String member) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zrevrank(key, member);
        } catch (Exception e) {
            closeJedis(jedis);
        }
        return -1;
    }


    /**
     * 删除 zset 集合中 给定的值
     *
     * @param key
     * @param member
     * @return
     */
    public static long zrem(String key, String... member) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zrem(key, member);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }

    /**
     * 删除 zset 集合
     *
     * @param key String 键
     * @return
     */
    public static long zrem(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }


    /**
     * 删除 zset集合 给定位置区间的元素
     *
     * @param key   String
     * @param start int 开始区间，从0开始(包含)
     * @param end   int 结束区间,-1为最后一个元素(包含)
     * @return 删除的数量
     */
    public static long zremrangeByRank(String key, int start, int end) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zremrangeByRank(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }


    /**
     * 删除 zset集合 给定权重区间的元素
     *
     * @param key String
     * @param min double 下限权重(包含)
     * @param max double上限权重(包含)
     * @return 删除的数量
     */
    public static long zremrangeByScore(String key, double min, double max) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zremrangeByScore(key, min, max);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }


    /**
     * 获取给定值在集合中的权重
     *
     * @param key    String
     * @param member String
     * @return double 权重
     */
    public static double zscore(String key, String member) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.zscore(key, member);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return -1;
    }
    /*---------------- zset 类型操作 end -------------------------*/

    /*--------------- 地理位置操作 begin -------------------------*/

    /**
     * 增加地理位置的坐标
     *
     * @param key        键
     * @param coordinate 地理位置坐标
     * @param memberName 位置名称
     * @return
     */
    public static Long geoadd(String key, GeoCoordinate coordinate, String memberName) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.geoadd(key, coordinate.getLongitude(), coordinate.getLatitude(), memberName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 批量添加地理位置
     *
     * @param key                 键
     * @param memberCoordinateMap 成员坐标图
     * @return 成功 1
     */
    public static Long geoadd(String key, Map<String, GeoCoordinate> memberCoordinateMap) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.geoadd(key, memberCoordinateMap);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 根据给定地理位置坐标获取指定范围内的地理位置集合（返回匹配位置的经纬度 + 匹配位置与给定地理位置的距离 + 从近到远排序，）
     *
     * @param key        键
     * @param coordinate 地理位置坐标
     * @param radius     半径 千米
     * @return List<GeoRadiusResponse>
     */
    public static List<GeoRadiusResponse> georadius(String key, GeoCoordinate coordinate, double radius) {
        Jedis jedis = null;
        List<GeoRadiusResponse> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.georadius(key, coordinate.getLongitude(), coordinate.getLatitude(), radius, GeoUnit.KM,
                    GeoRadiusParam.geoRadiusParam().withDist().withCoord().sortAscending());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 根据给定地理位置获取指定范围内的地理位置集合（返回匹配位置的经纬度 + 匹配位置与给定地理位置的距离 + 从近到远排序，）
     *
     * @param key    键
     * @param member 地理位置
     * @param radius 半径 千米
     * @return List<GeoRadiusResponse>
     */
    public static List<GeoRadiusResponse> georadiusByMember(String key, String member, double radius) {
        Jedis jedis = null;
        List<GeoRadiusResponse> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.georadiusByMember(key, member, radius, GeoUnit.KM, GeoRadiusParam.geoRadiusParam().withDist().withCoord().sortAscending());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }

    /**
     * 查询两位置距离
     *
     * @param key     键
     * @param member1 地理位置1
     * @param member2 地理位置2
     * @param unit    单位
     * @return 距离
     */
    public static Double geoDist(String key, String member1, String member2, GeoUnit unit) {
        Jedis jedis = null;
        Double result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.geodist(key, member1, member2, unit);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 可以获取某个地理位置的geohash值
     *
     * @param key     键
     * @param members 地理位置
     * @return
     */
    public static List<String> geohash(String key, String... members) {
        Jedis jedis = null;
        List<String> result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.geohash(key, members);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return result;
    }


    /**
     * 获取地理位置的坐标
     *
     * @param key     键
     * @param members 地理位置
     * @return
     */
    public static List<GeoCoordinate> geopos(String key, String... members) {
        Jedis jedis = null;
        try {
            jedis = RedisPool.getJedis();
            return jedis.geopos(key, members);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeJedis(jedis);
        }
        return null;
    }

    /*--------------- 地理位置操作 end-----------------*/


    /**
     * 关闭释放 jedis
     *
     * @param jedis jedis对象
     */
    private static void closeJedis(Jedis jedis) {
        if (jedis != null) {
            RedisPool.close(jedis);
        }
    }

}
