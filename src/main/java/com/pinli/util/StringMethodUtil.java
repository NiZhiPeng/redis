package com.pinli.util;


/**
 * 通过属性名返回属性的get/set方法
 */
public class StringMethodUtil {

    /**
     * 通过属性名获取get 方法
     *
     * @param fieldName 属性名
     * @return get方法名
     */
    public static String getMethodName(String fieldName) {
        if (fieldName == null) {
            return null;
        } else {
            return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        }
    }


    /**
     * 通过属性名获取get 方法
     *
     * @param fieldName 属性名
     * @return set方法名
     */
    public static String setMethodName(String fieldName) {
        if (fieldName == null) {
            return null;
        } else {
            return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        }
    }
}
