package com.pinli.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtils {

    /**
     * Object 对象转为JSON 字符串
     *
     * @param obj object 对象
     * @return Json 字符串
     */
    public static String objectToJson(Object obj) {
        if (obj == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T jsonToObject(String jsonVal, Class<T> tClass) {
        if (jsonVal == null) {
            return null;
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonVal, tClass);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


}
