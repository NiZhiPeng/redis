package com.pinli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PinliRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(PinliRedisApplication.class, args);
    }

}
