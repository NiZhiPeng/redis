package com.pinli;

import com.pinli.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * redis String 类型数据测试类
 */
@SpringBootTest
public class RedisStringTest {
    @Test
    void set() {
        System.out.println(RedisUtil.set("key1", "hello"));
        System.out.println(RedisUtil.set("key1", "hello"));
    }


    @Test
    void set8() {
        System.out.println(RedisUtil.set("8", "key1", "hello数据库"));
    }

    @Test
    void set8Byte() {
        String str = new String(RedisUtil.setByte("8", "byte1", "hello"));
        System.out.println(str);
    }


    @Test
    void get() {
        System.out.println(RedisUtil.get("key1"));
    }

    @Test
    public void type() {
        System.out.println(RedisUtil.type("key1"));
        System.out.println(RedisUtil.type("key2"));
        System.out.println(RedisUtil.type("key3"));
        System.out.println(RedisUtil.type("keyList"));
        System.out.println(RedisUtil.type("keyZset"));
    }

    @Test
    void strlen() {
        System.out.println(RedisUtil.strlen("key122"));
    }

    @Test
    void append() {
        System.out.println(RedisUtil.append("key1233", "123"));
    }

    @Test
    void getrange() {
        System.out.println(RedisUtil.getrange("key1233", 2, 5));
    }

    @Test
    void setrange() {
        System.out.println(RedisUtil.setrange("key1233", 14, "set"));
    }


}
