package com.pinli;

import com.pinli.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;
import java.util.Set;

@SpringBootTest
public class RedisHashTest {
    @Test
    void hset() {
/*        System.out.println(RedisUtil.hset("key2", "name", "测试"));
        System.out.println(RedisUtil.hset("key2", "age", "20"));
        System.out.println(RedisUtil.hset("key2", "sex", "女"));*/

        System.out.println(RedisUtil.hset("hashKey", "1", "0.1"));

    }


    @Test
    void hget() {
        System.out.println(RedisUtil.hget("key2", "name"));
        System.out.println(RedisUtil.hget("key2", "age"));
        System.out.println(RedisUtil.hget("key23", "ages")); //null
    }

    @Test
    public void hmget() {
        String[] field = {"name", "sex", "age"};
        String[] field2 = {"age", "name"};
        System.out.println(RedisUtil.hmget("key2", field));
        System.out.println(RedisUtil.hmget("key2", field2));
    }

    @Test
    public void hgetAll() {
        Map<String, String> result = RedisUtil.hgetAll("key2");
//        Set s = result.entrySet();
        for (String key : result.keySet()) {
            System.out.println(key);
            System.out.println(result.get(key));
        }

    }

    @Test
    public void hdel() {
        Long re = RedisUtil.hdel("key2", "sex", "age");
        System.out.println(re);
        Map<String, String> result = RedisUtil.hgetAll("key2");
//        Set s = result.entrySet();
        for (String key : result.keySet()) {
            System.out.println(key + "  :  " + result.get(key));
        }

    }

    @Test
    public void hkeys() {
        Set<String> re = RedisUtil.hkeys("key2");
        for (String i : re) {
            System.out.println(i);
        }
    }

}
