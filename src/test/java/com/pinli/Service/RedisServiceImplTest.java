package com.pinli.Service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class RedisServiceImplTest {
    @Autowired
    private RedisService redisService;


    @Test
    void hasKey() {
    }

    @Test
    void expire() {
    }

    @Test
    void remove() {
    }

    @Test
    void set() {
    }

    @Test
    void getObject() {
    }

    @Test
    void setEx() {
    }

    @Test
    void addRedisSet() {
    }

    @Test
    void getRedisSet() {
    }

    @Test
    void getRedisSetSize() {
    }

    @Test
    void addHash() {
    }

    @Test
    void getObjectByRedis() {
    }

    @Test
    void hset() {
    }

    @Test
    void hget() {
    }

    @Test
    void addList() {
        List<String> strs = new ArrayList<>();
        strs.add("第一");
        strs.add("第二");
        System.out.println(redisService.addList("listKey",strs,10L));
    }

    @Test
    void getList() {
        System.out.println(redisService.getList("listKey"));
    }


    @Test
    void setBytes() {
        System.out.println(TimeUnit.MINUTES);
        System.out.println(TimeUnit.MINUTES.toSeconds(5));

    }

    @Test
    void getBytes() {
    }
}