package com.pinli;

import com.pinli.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Iterator;
import java.util.Set;

@SpringBootTest
public class RedisListTest {
    @Test
    void lpush() {
//        RedisUtil.del("ListKey");
        String[] str = {"1", "2"};
        System.out.println(RedisUtil.lpush("ListKey", str));
        System.out.println(RedisUtil.rpush("ListKey2", str));

    }

    @Test
    void lrange() {
//        RedisUtil.del("ListKey");
        String[] str = {"1", "2"};
        System.out.println(RedisUtil.lrange("ListKey", 0, 2));
        System.out.println(RedisUtil.lrange("ListKey2", 0, 2));

    }

    @Test
    void keysPre() {
//        RedisUtil.del("ListKey");
        String[] str = {"1", "2"};
//        System.out.println(RedisUtil.keysPre("ListKey", str));

        Set<String> set = RedisUtil.keys("ListKey*");
        Iterator it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        for (String stri : set) {
            System.out.println(stri);
        }


    }
}
