package com.pinli;

import com.pinli.util.RedisUtil;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class RedisZsetTest {


    @Test
    public void zadd() {
        //zset添加单个数据
        System.out.println(RedisUtil.zadd("keyZset", 1, "测试不可重复集合"));
    }

    @Test
    public void zadd2() {
        Map<String, Double> map = new HashMap<String, Double>();
        map.put("a3",- 1.4);
        map.put("a5", 1.5);
        map.put("a6", 1.6);
        map.put("a7", 1.7);
        map.put("a8", 1.8);
        map.put("a9", 1.9);
        map.put("a10", 2.0);
        System.out.println(RedisUtil.zadd("keyZset", map));
    }

    @Test
    public void zcard() {
        System.out.println(RedisUtil.zcard("keyZset"));
    }

    @Test
    public void zcount() {
        System.out.println(RedisUtil.zcount("keyZset", 1.1, 1.3));
    }

    @Test
    public void zrange() {
        System.out.println(RedisUtil.zrange("keyZset", 1, 3));
    }



    @Test
    public void zrangeByScore() {
        System.out.println(RedisUtil.zrangeByScore("keyZset", 1.4, 1.8));
    }



    @Test
    public void zrevrange() {
        System.out.println(RedisUtil.zrevrange("keyZset", 1, 3));
    }



    @Test
    public void zrank() {
        System.out.println(RedisUtil.zrank("keyZset", "a5"));
    }


    @Test
    public void zscore() {
        System.out.println(RedisUtil.zscore("keyZset", "a5"));
    }

    @Test
    public void  zrevrank(){
        System.out.println(RedisUtil.zrevrank("keyZset","a5"));
    }

    @Test
    public void  zrem(){
        System.out.println(RedisUtil.zrem("keyZset","a5"));
    }


    @Test
    public void  zrem2(){
        System.out.println(RedisUtil.zrem("keyZset"));
    }


    @Test
    public void  zrem3(){
        System.out.println(RedisUtil.del("keyZset"));
    }
    @Test
    public void  zremrangeByRank(){
        System.out.println(RedisUtil.zremrangeByRank("keyZset",1,4));
    }
    @Test
    public void  zremrangeByScore(){
        System.out.println(RedisUtil.zremrangeByScore("keyZset",1.4,1.5));
    }

}
