package com.pinli;

import com.pinli.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
class PinliRedisApplicationTests {



    @Test
    public void getJedisBoolean() {
        System.out.println(RedisUtil.getJedisBoolean());
        System.out.println(RedisUtil.getJedisBoolean());
    }



    @Test
    void redsiKeys() {
        Set<String> keys = RedisUtil.keys("key*");
        for (String key : keys) {
            System.out.println(key);
        }
    }



    @Test
    public void sadd() {
        System.out.println(RedisUtil.sadd("key3", "b"));
    }

    @Test
    public void smembers() {
        Set<String> result = RedisUtil.smembers("key3");
        for (String s : result) {
            System.out.println(s);
        }
    }

    @Test
    public void scard() {
        System.out.println(RedisUtil.scard("key3"));
    }

    @Test
    public void exists() {
        System.out.println(RedisUtil.exists("key6"));
    }

    @Test
    public void lpush() {
        System.out.println(RedisUtil.lpush("keyList", "2", "1", "123"));
    }

    @Test
    public void lrange() {
        System.out.println(RedisUtil.lrange("keyList", 0, 5));
    }

    @Test
    public void llen() {
        System.out.println(RedisUtil.llen("keyList"));
    }

    @Test
    public void del() {
        System.out.println(RedisUtil.del("keyList"));
    }


    @Test
    public void expire() {
        System.out.println(RedisUtil.expire("keyList", 30));
    }

    @Test
    public void pexpireAt() {
        Long min = System.currentTimeMillis() + 1000 * 30;
        System.out.println(RedisUtil.pexpireAt("keyList", min));
    }

    @Test
    public void ttl() {
        System.out.println(RedisUtil.ttl("keyList"));
    }

}
